#include <stdio.h> 
main() 
{ 
int n, p, q; 
n = 5; p = 2; 
q = n++ > p || p++ !=3; /*  E:wypisz n, p, q */ 
printf("A: n=%d p=%d q=%d\n ", n,p,q);
n = 5; p = 2; 
q = n++ <p || p++ !=3; /* F: wypisz n, p, q */
printf("B: n=%d p=%d q=%d\n ", n,p,q);
n = 5; p = 2; 
q = ++n == 3 && ++p == 3; /* G: wypisz n, p, q */ 
printf("C: n=%d p=%d q=%d\n ", n,p,q);
n = 5; p = 2; 
q = ++n == 6 && ++p == 3; /*  H: wypisz n, p, q */
printf("D: n=%d p=%d q=%d\n ", n,p,q);
}
