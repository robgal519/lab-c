/*  Robert Gałat    WFiIS IS    */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
//  -------------------stałe i zmienne globalne--------------------------
typedef struct Klient
{
   char imie[20], nazwisko[32];
   char kod[7];
   int nrtel;
}etykieta;
//  ----------------------prototypy funkcji------------------------------
void wypisz_strukt(etykieta *);
//  -------------------------funkcja main--------------------------------
int main(int argc, char **argv)
{
    if(argc!=3)return EXIT_FAILURE;

    FILE *plik_txt=fopen(argv[1],"r");
    FILE *plik_bin=fopen(argv[2],"wb");
    etykieta klient;
    while(!feof(plik_txt))
    {
         
        fscanf(plik_txt,"%s\t%s\t%s\t%d\n",klient.imie,klient.nazwisko,klient.kod,&klient.nrtel);
        fwrite(&klient, sizeof(klient),1,plik_bin);
    }

    fclose(plik_txt);
    fclose(plik_bin);
    
    plik_bin=fopen(argv[2],"rb");
    while(!feof(plik_bin))
    {
     fread(&klient,sizeof(klient),1,plik_bin);
        if(!feof(plik_bin))
            wypisz_strukt(&klient);

    }

return 0;
}
//  ----------------------------funkcje----------------------------------
void wypisz_strukt(etykieta *a)
{
     printf("%s\t%s\t%s\t%d\n",a->imie,a->nazwisko,a->kod,a->nrtel);
}

//  -----------------------------testy-----------------------------------


