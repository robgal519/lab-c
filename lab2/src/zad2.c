#include <stdio.h>
int main(void)
{
int arg1,arg2;
char operator;
scanf("%d%c%d", &arg1,&operator,&arg2);
switch(operator){
case '+':
    printf("%d+%d=%d",arg1,arg2,arg1+arg2);
    break;
case '-':
    printf("%d-%d=%d",arg1,arg2,arg1-arg2);
    break;
case '*':
    printf("%d*%d=%d",arg1,arg2,arg1*arg2);
    break;
case '/':
    if(arg2!=0)
        printf("%d/%d=%f",arg1,arg2,(float)arg1/(float)arg2);
    break;
}

return 0;}
