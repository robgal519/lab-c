#include <stdio.h>
#include <time.h>

#define size 10


int main(void)
{
time_t date;
srand(time(&date));

char znak,licznik;
char  tablica[size];
// wypelnienie tablicy znakami
for(licznik=0;licznik<size;licznik++)
{
    znak=rand()%57+65;
    while(znak<97 && znak>90)
        znak=rand()%57+65;
    tablica[licznik]=znak;
    printf("%c  ",znak);
}
printf("\n");
// zmiana wielkosci liter
for(licznik=0;licznik<size;licznik++)
{
    if(tablica[licznik]>96)
        {tablica[licznik]-=32;}
    else
        {tablica[licznik]+=32;}

    printf("%c  ",tablica[licznik]);
}
printf("\n");
// wypisz od konca
for(licznik=0;licznik<size;licznik++)
{
    printf("%c  ",tablica[size-licznik-1]);
}
return 0;
}
