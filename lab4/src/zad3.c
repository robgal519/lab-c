#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define size 10

// funkcje
int ra(int,int);
float funkcja(float *t, int tab_size, float *max, float *min, float *srednia, float *odchylenieMax, float *odchylenieMin);
float roznica(float *arg1,float *arg2);
void swap(int*a, int *b);
void bouble_sort(int *tab,int s);

// koniec funkcji



// MAIN
int main(void)
{
srand(time(NULL));
    float tab[size];
    int min,max;
    float maxt,mint,srednia,odchMax,odchMin,suma;
   scanf("%d,%d",&min,&max);
    float *wsk=tab;
    for(;wsk<tab+size;wsk++)
    {
        *wsk=ra(min,max);
        printf("%f\n",*wsk);
    }
    suma=funkcja(&tab[0],size,&maxt,&mint,&srednia,&odchMax,&odchMin);
    printf("suma\tmax\tmin\tsrednia\tmax odchylenie\tMin odchylenie\n");
    printf("%f\t%f\t%f\t%f\t%f\t%f\n",suma,maxt,mint,srednia,odchMax,odchMin);
    
    int tablica[size];
    int i;
    
    for(i=0;i<size;i++)
        {
            tablica[i]=rand()%50;
            printf("%d\t",tablica[i]);
        }
    printf("\n");
    bouble_sort(tablica,size);
    for(i=0;i<size;i++)
    {
        printf("%d\t",tablica[i]);
    }

     


return 0;
}
// funkcje
int ra(int A,int B)
{
    int range=B-A;
return (rand()%range)+A;
}

float funkcja(float *t, int tab_size, float *max, float *min, float *srednia, float *odchylenieMax, float *odchylenieMin)
{
float suma;
*max=*min=*t;
int i=0;
for(;i<tab_size;i++)
{
    suma+=*(t+i);   
    if(*(t+i)>*max)
        *max=*(t+i);
    if(*(t+i)<*min)
        *min=*(t+i);
}
*srednia=suma/tab_size;

*odchylenieMax=*odchylenieMin=roznica(srednia,t);
for(i=0;i<tab_size;i++)
{
    if(roznica((t+i),srednia)<*odchylenieMin)
        *odchylenieMin=roznica((t+i),srednia);
    if(roznica((t+i),srednia)>*odchylenieMax)
        *odchylenieMax=roznica((t+i),srednia);
}
return suma;
}

float roznica(float *arg1,float *arg2)
{
    if(*arg1<*arg2)
        return *arg2-*arg1;
    else
        return *arg1-*arg2;
}


// zad c

void bouble_sort(int *tab,int s)

{
int i=s-1;
int j;
    for(;i>=0;i--)
    {
        for(j=1;j<=i;j++)
        {
            if(*(tab+j-1)>*(tab+j))
                swap((tab+j-1),(tab+j));
        }
    }
}

void swap(int *a, int *b)
{
int c=*a;
*a=*b;
*b=c;
}
