/*  Robert Gałat    WFiIS IS    */

#include <stdio.h>
#include <time.h>
#include <limits.h>

//  -------------------stałe i zmienne globalne--------------------------
#define zakres 100000
//  ----------------------prototypy funkcji------------------------------

void fill_int_array(int *A, int size);
void print_array(int *t,int size);
void sito(int *t,int size);

int prime_test(int *t, int size);

//  -------------------------funkcja main--------------------------------
int main(void)
{
int OK=1;
int tablica[zakres];
fill_int_array(tablica, zakres);
  
tablica[0]=0;
tablica[1]=0;

sito(tablica,zakres);

printf("\n prime_test\t");
if(!prime_test(tablica,zakres)) printf("OK\n"); else {printf("fail\n"); OK=0;}

// wypisz liczby pierwsze 
if(OK)
    print_array(tablica, zakres);
return 0;
}
//  ----------------------------funkcje----------------------------------
void fill_int_array(int *A, int size)
{
for(--size;size>=0;size--)
    {
        *(A+size)=size;
    }
}
void print_array(int *t,int size)
{
int i=0;
for(;i<size;i++)
    if(*(t+i))
        printf("%d\t",*(t+i));       
}

void sito(int *t,int size)
{
int i,j;
for(i=2;i*i<size;i++)
{
    if(*(t+i))
    {
        for(j=i+1;j<size;j++)
        {
            if(*(t+j)&&!(*(t+j)%i))
            {
                *(t+j)=0;
            }
        }
    }
}
}
//  -----------------------------testy-----------------------------------


int prime_test(int *t, int size)
{
int j,i=0;
    for(;i<size;i++)
    {
        if(*(t+i)>2)
        {
            for(j=2;j*j<*(t+i);j++)
                if(!(i%j))
                {     
                return 1;
                }
        }

    }
return 0;
}
