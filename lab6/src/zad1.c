/*  Robert Gałat    WFiIS IS    */

#include <stdio.h>
#include <time.h>
//  -------------------stałe i zmienne globalne--------------------------
//  ----------------------prototypy funkcji------------------------------
//  -------------------------funkcja main--------------------------------
int main(void)
{
int T[10][5];
int i,j,k=2;

// wypełniij tablicę
for(i=0;i<10;i++)
{
     for(j=0;j<5;j++)
     {
         T[i][j]=k;
         k+=2;
     }
}
// wypisz tablicę
for(i=0;i<10;i++)
{
     for(j=0;j<5;j++)
     {
         printf("%d, %p\t",T[i][j],&T[i][j]);
     }
     printf("\n");
}
printf("T[0]= %d, %p \n",*(*(T+0)+0), *(T+0)+0);
printf("T[0]+1= %d, %p\n", *(*(T+0)+1),*(T+0)+1);
printf("T: %d, %p\n",**T,*T);
printf("T+1: %d, %p \n",*(*(T+0)+1), *T+1);

printf("\n");

for(i=0;i<10;i++)
{
     for(j=0;j<5;j++)
     {
         printf("%d, %p\t",*(*(T+i)+j), *(T+i)+j);
     }
     printf("\n");
}
printf("\n\n");
int (*wsk2d)[5];
wsk2d=T;
for(i=0;i<10;i++)
{
     for(j=0;j<5;j++)
     {
         printf("%d, %p\t",*(*(wsk2d+i)+j), *(wsk2d+i)+j);
     }
    printf("\n");
}

return 0;
}
//  ----------------------------funkcje----------------------------------
//  -----------------------------testy-----------------------------------

