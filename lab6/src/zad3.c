/*  Robert Gałat    WFiIS IS    */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//  -------------------stałe i zmienne globalne--------------------------
#define size 5
//  ----------------------prototypy funkcji------------------------------
void sumy(int (*wsk)[size], int *sm,int *s1, int *s2, int *s3 );

int main(void)
{
int T[size][size];
int suma,suma1,suma2,suma3;

int i,j;
srand(time(NULL));
for(i=0;i<size;i++)
{
     for(j=0;j<size;j++)
     {
         T[i][j]=rand()%20;
         printf("%d  ",T[i][j]);
     }
     printf("\n");
}
printf("\n\n");
sumy(T,&suma,&suma1,&suma2,&suma3);
printf("suma: %d, przekatna: %d, nad przekątną: %d, pod przekątną: %d",suma,suma3,suma1,suma2);

return 0;
}
//  ----------------------------funkcje----------------------------------
void sumy(int (*wsk)[size], int *sm,int *s1, int *s2, int *s3)
{
     int i,j;
     
     *sm=*s1=*s2=*s3=0;
     for(i=0;i<size;i++)
     {
         for(j=0;j<size;j++)
         {
             *sm+=*(*(wsk+i)+j);
             if(i==j)    *s3+=*(*(wsk+i)+j);
             if(i>j)    *s2+=*(*(wsk+i)+j);
             if(i<j)    *s1+=*(*(wsk+i)+j);
             printf("%d  ",wsk[i][j]);
         }
         printf("\n");
     }
}
//  -----------------------------testy-----------------------------------

