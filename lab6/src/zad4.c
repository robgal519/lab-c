/*  Robert Gałat    WFiIS IS    */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//  -------------------stałe i zmienne globalne--------------------------
#define size 8
//  ----------------------prototypy funkcji------------------------------
float ra(float min,float max);
//  -------------------------funkcja main--------------------------------
int main(void)
{
float T1[size][size];
float T2 [size][size];
float T3[size][size];
int i, j;
for(i=0;i<size;i++)
{
     for(j=0;j<size;j++)
     {
         T1[i][j]=ra(12.5,44);
         T2[i][j]=ra(12.5,44);
         printf("%.2f ", T1[i][j]);
     }
     printf("\n");
}
printf("\n\n");
for(i=0;i<size;i++)
{
     for(j=0;j<size;j++)
     {
         printf("%.2f ", T2[i][j]);
     }
     printf("\n");
}

    
return 0;

}
//  ----------------------------funkcje----------------------------------
float ra(float min,float max)
{
    static int i=0;
    if(!i)
    {
        i++;    
        srand(time(NULL));
    }
     return (rand()/(float)RAND_MAX)*(max-min)+min;
}
//  -----------------------------testy-----------------------------------

