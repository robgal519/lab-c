/*  Robert Gałat    WFiIS IS    */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//  -------------------stałe i zmienne globalne--------------------------
//  ----------------------prototypy funkcji------------------------------
//  -------------------------funkcja main--------------------------------
int main(void)
{
float **tab;
float suma1=0;
int i,j;
int rozmiar;
scanf("%d",&rozmiar);
tab=(float**)malloc(sizeof(*tab)*rozmiar);
for(i=0;i<rozmiar;i++)
{
     tab[i]=(float*)malloc(sizeof(**tab)*rozmiar);
}
if(!tab)exit(EXIT_FAILURE);
srand(time(NULL));
for(i=0;i<rozmiar;i++)
{
     for(j=0;j<rozmiar;j++)
     {
         tab[i][j]=(rand()/(float)RAND_MAX)*30;
         printf("\t%.2f",tab[i][j]);
         suma1+=tab[i][j];
     }
printf("\n");
}
printf("\n\n%f",suma1);
for(i=0;i<rozmiar;i++)
free(tab[i]);
free(tab);
return 0;
}
//  ----------------------------funkcje----------------------------------
//  -----------------------------testy-----------------------------------

