/*  Robert Gałat    WFiIS IS    */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
//  -------------------stałe i zmienne globalne--------------------------
//  ----------------------prototypy funkcji------------------------------
void stats(char *tab);
//  -------------------------funkcja main--------------------------------
int main(void)
{
char linia[100];
fgets(linia,sizeof(linia),stdin);
stats(linia);
printf("\n%s", linia);
return 0;
}
//  ----------------------------funkcje----------------------------------
void stats(char *tab)
{
int male=0,duze=0,cyfra=0,inter=0,biale=0;
     
     while(*tab)
     {
         if(isdigit(*tab))  cyfra++;
         if(islower(*tab))  male++;
         if(isupper(*tab))  duze++;
         if(isspace(*tab))  biale++;
         if(ispunct(*tab))  inter++;
         tab++;
     }
printf("\n\n male litery: %d; duze litery: %d; cyfry: %d; znaki interpunkcyjne: %d; znaki białe: %d",male,duze,cyfra,inter,biale);
}
//  -----------------------------testy-----------------------------------

