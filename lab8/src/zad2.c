/*  Robert Gałat    WFiIS IS    */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
//  -------------------stałe i zmienne globalne--------------------------
//  ----------------------prototypy funkcji------------------------------
int wordCounter(char *tab);
//  -------------------------funkcja main--------------------------------
int main(void)
{
char linia[100];
fgets(linia,sizeof(linia),stdin);
printf("wyrazów : %d",wordCounter(linia));
    
return 0;
}
//  ----------------------------funkcje----------------------------------
int wordCounter(char *tab)
{
int counter=0;
     while(*(tab+1))
     {
         if(isalnum(*(tab)) && !isalnum(*(tab+1))) counter++;
         tab++;
     }
return counter;
}
//  -----------------------------testy-----------------------------------

