/*  Robert Gałat    WFiIS IS    */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
//  -------------------stałe i zmienne globalne--------------------------
//  ----------------------prototypy funkcji------------------------------
char losuj(int,int);
//  -------------------------funkcja main--------------------------------
int main(void)
{
    char losuj_wyraz[10];
    int i=0;
    for(;i<10;i++)
    {
         losuj_wyraz[i]=losuj(97,122);
    }
    char zmienna=losuj((int)'a',(int)'z');
    int first;
    int isf=0;
    int count=0;
    for(i=0;i<10;i++)
    {
         if(losuj_wyraz[i]==zmienna)
         {
             if(!isf)
             {  
                isf++;
                first=i;
                count++;
             }else
                count++;
         }
    }
    printf("%s\t%c\t%i\t%i\n",losuj_wyraz,zmienna,first,count);
return 0;
}
//  ----------------------------funkcje----------------------------------

char losuj(int a,int b)
{
    static int i=0;
     if(!i)
    {
         srand(time(NULL));
         i++;
    }
    return (char)(rand()%(b-a))+a;
}
//  -----------------------------testy-----------------------------------

