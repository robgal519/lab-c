/*  Robert Gałat    WFiIS IS    */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
//  -------------------stałe i zmienne globalne--------------------------
#define BufSize 1024
//  ----------------------prototypy funkcji------------------------------
//  -------------------------funkcja main--------------------------------
int main(int argc, char **argv)
{
FILE *inFile= fopen(argv[1],"r+");
FILE *outFile= fopen("out.dat","w");
char slowo[]="wagon";
if(inFile==NULL) perror ("error opening file");
char buffer[BufSize];
int ok=0;
while (!feof(inFile))
{

    if(fgets(buffer,BufSize,inFile)==NULL) break;
    int i=0,j=0;
    for(;i<BufSize-strlen(slowo);i++)
        {ok=0;
             for(j=0;j<strlen(slowo);j++)
             {
                 if(*(buffer+i+j)!=*(slowo+j))
                    ok++;
             }
        if(!ok)
            fputs(buffer,outFile);
        }
}

fclose(inFile);
fclose(outFile);
return 0;
}
//  ----------------------------funkcje----------------------------------
//  -----------------------------testy-----------------------------------

