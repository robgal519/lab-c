/* ptrfn.c */
/* Tablica wskaznikow do funkcji */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

double ff(double x){
	return 1+x*x;
}
double df(double x)
{
     if(x)
     {
         return 1.0/x;
     }else return EXIT_FAILURE;
}
int main(void){
    srand(time(NULL));
    double (*p[])(double) = {sin,cos,ff,df,NULL};
	double (**q)(double) = p;
    int i=0;
    int j=0;
	for(j=0;j<10;j++)
        {
            while(*q)  printf("%.2f\t", (*(*q++))(rand()%20));
                    q=p;
               
            printf("\n");
            i=0;
        }
	return 0;
}
