/*  Robert Gałat    WFiIS IS    */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
//  -------------------stałe i zmienne globalne--------------------------
//  ----------------------prototypy funkcji------------------------------
int add2(int *, int *);
int comp2(int *, int *);
int add2_or_comp2(int,int,int(int*, int*));
//  -------------------------funkcja main--------------------------------
int main(int argc, char **argv)
{
    
int r,l;
r=7;
l=9;
printf("%d\n",add2_or_comp2(r,l,add2));
printf("%d\n",add2_or_comp2(r,l,comp2));
return 0;
}
//  ----------------------------funkcje----------------------------------
int add2(int *a,int *b)
    {
    return *a+*b;}
int comp2(int *a, int *b)
    {
    return *a-*b;};
int add2_or_comp2(int a,int b, int funk(int*,int*))
{
     return(funk(&a,&b));
}
//  -----------------------------testy-----------------------------------


